<?php
/**
 * @file
 * Drush commands for translations import/export.
 *
 * Provides Drush commands to import and export Gettext Portable Object (.po)
 * and Gettext Portable Object Template (.pot) for translatable strings in the
 * local Drupal database.
 *
 * TODO: Add options for PO(T) header information (project name, version, etc.).
 * TODO: Add option for PO(T) files base name (currently hardcoded to 'drupal').
 */

/**
 * Implements COMMANDFILE_drush_command().
 */
function locale_drush_command() {
  return array(
    'locale-export-templates' => array(
      'command hook' => 'export_templates',
      'description' => 'Generate Gettext Portable Object Template (.pot) files for all strings from the Drupal locale database.',
      'arguments' => array(
        'groups' => "Text groups to export POT file for (eg. 'default' for interface translations)",
      ),
      'required-arguments' => FALSE,
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'drupal dependencies' => array('locale'),
      'options' => array(
        'destination' => array(
          'description' => ' The full path in which the templates files should be stored. If omitted, it will be saved to the sites/all/translations folder.',
        ),
      ),
    ),
    'locale-export-translations' => array(
      'command hook' => 'export_translations',
      'description' => 'Generate Gettext Portable Object (.po) files with all strings from the Drupal locale database.',
      'arguments' => array(
        'groups' => "Text groups to export translations files from (eg. 'default' for interface translations)",
      ),
      'required-arguments' => FALSE,
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'drupal dependencies' => array('locale'),
      'options' => array(
        'destination' => array(
          'description' => ' The full path in which the translations files should be stored. If omitted, it will be saved to the sites/all/translations folder.',
        ),
      ),
    ),
    'locale-import-translations' => array(
      'command hook' => 'import_translations',
      'description' => 'Import translations from Gettext Portable Object Template (.po) files.',
      'arguments' => array(
        'groups' => "Text groups to import translations files to (eg. 'default' for interface translations)",
      ),
      'required-arguments' => FALSE,
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'drupal dependencies' => array('locale'),
      'options' => array(
        'source' => array(
          'description' => 'The full path in which the imported translations files are stored. If omitted, the sites/all/translations folder will be used.',
        ),
        'mode' => array(
          'description' => "Should existing translations be replaced ('keep') or overwritten ('overwrite') (default to 'keep')",
        ),
      ),
    ),
  );
}

/**
 * Command callback for the locale-import-translations command.
 *
 * TODO: Use _locale_import_po() instead of _l10n_update_locale_import_po().
 * This requires figuring how to retrieve import result (adds, deletes, etc.).
 *
 */
function drush_locale_import_translations() {
  $groups = drush_get_arguments();
  // Remove the command name form the groups.
  array_shift($groups);
  // Validate groups.
  if (!($groups = _drush_locale_groups($groups))) {
    return;
  }

  $source = drush_get_option('source', 'sites/all/translations');
  if (!is_dir($source) || !is_readable($source)) {
    drush_set_error(dt('Directory @source is not readable', array('@source' => $source)));
    return;
  }

  $mode = drush_get_option('mode', 'keep');
  $mode_name = 'LOCALE_IMPORT_' . drupal_strtoupper($mode);
  if (!defined($mode_name)) {
    drush_set_error(dt('Invalid import mode: @mode', array('@mode' => $mode)));
    return;
  }
  else {
    drush_log("Import mode set to $mode_name", 'notice');
    $mode = constant($mode_name);
  }
  $languages = language_list();
  $total = array();
  foreach ($languages as $langcode => $language) {
    $total[$langcode] = array(
      'add' => 0,
      'update' => 0,
      'delete' => 0,
      'skip' => 0,
    );
    foreach ($groups as $group) {
      $pofile = ($group !== 'default') ? "drupal.$group.$langcode.po" : "drupal.$langcode.po";
      if (file_exists("$source/$pofile")) {
        drush_log(dt("Importing @language translations for text group '@group' from @source/@pofile", array(
          '@language' => $language->name,
          '@group' => $group,
          '@source' => $source,
          '@pofile' => $pofile,
        )), 'status');
        $file = new stdClass();
        $file->uri = "$source/$pofile";
        $file->filename = basename("$source/$pofile");
        if ($result = _drush_locale_import_po($file, $langcode, $mode, $group)) {
          foreach ($result as $key => $value) {
            $total[$langcode][$key] += $value;
          }
        }
      }
    }
    drush_log(dt('@language translation strings added: !add, updated: !update, deleted: !delete, skipped: !skip.', array(
      '@language' => $languages[$langcode]->name,
      '!add' => $total[$langcode]['add'],
      '!update' => $total[$langcode]['update'],
      '!delete' => $total[$langcode]['delete'],
      '!skip' => $total[$langcode]['skip'],
    )), 'status');
  }
}

/**
 * Command callback for the locale-export-translations command.
 */
function drush_locale_export_translations() {
  $groups = drush_get_arguments();
  // Remove the command name form the groups.
  array_shift($groups);
  // Validate groups.
  if (!($groups = _drush_locale_groups($groups))) {
    return;
  }
  $destination = drush_get_option('destination', 'sites/all/translations');

  // Create destination dir.
  if (!drush_mkdir($destination) && !drush_get_context('DRUSH_SIMULATE')) {
    drush_set_error(dt('Failed to create directory @destination', array('@destination' => $destination)));
    return;
  }
  if (!is_dir($destination) || !is_writable($destination)) {
    drush_set_error(dt('Directory @destination is not writable', array('@destination' => $destination)));
    return;
  }
  $languages = language_list();

  foreach ($languages as $language) {
    _drush_local_export($language, $groups, $destination);
  }
}

/**
 * Command callback for the locale-export-templates command.
 */
function drush_locale_export_templates() {
  $groups = drush_get_arguments();
  // Remove the command name form the groups.
  array_shift($groups);
  // Validate groups.
  if (!($groups = _drush_locale_groups($groups))) {
    return;
  }
  $destination = drush_get_option('destination', 'sites/all/translations');

  // Create destination dir.
  if (!drush_mkdir($destination) && !drush_get_context('DRUSH_SIMULATE')) {
    drush_set_error(dt('Failed to create directory @destination', array('@destination' => $destination)));
    return;
  }
  if (!is_dir($destination) || !is_writable($destination)) {
    drush_set_error(dt('Directory @destination is not writable', array('@destination' => $destination)));
    return;
  }

  _drush_local_export(NULL, $groups, $destination);
}

/**
 * Generates the PO(T) files for given text groups and language.
 *
 * A file will be generated for each text groups in the given destination
 * directory.
 *
 * @param stdClass $language
 *   The Drupal language object for language. Or NULL to generate templates.
 * @param array $groups
 *   An array of text groups as strings.
 * @param string $destination
 *   The path where to generates files.
 */
function _drush_local_export($language, $groups, $destination) {
  if (module_exists('i18n_string')) {
    module_load_include('admin.inc', 'i18n_string');
    foreach ($groups as $group) {
      i18n_string_refresh_group($group, $delete = FALSE);
    }
  }
  if ($language === NULL) {
    $langcode = NULL;
    $ext = "pot";
    $message = "Exporting translations template for text group '@group' to @destination/@filename";
  }
  else {
    $langcode = $language->language;
    $ext = "{$langcode}.po";
    $message = "Exporting @language translations for text group '@group' to @destination/@filename";
  }
  foreach ($groups as $group) {
    $filename = ($group !== 'default') ? "drupal.$group.$ext" : "drupal.$ext";
    drush_log(dt($message, array(
      '@language' => ($language === NULL) ? dt('Template') : $language->name,
      '@group' => $group,
      '@destination' => $destination,
      '@filename' => $filename,
    )), 'status');
    file_put_contents("$destination/$filename", _locale_export_po_generate($langcode, _locale_export_get_strings($langcode, $group)));
  }
}

/**
 * Validates a list of text groups.
 *
 * @param array $groups
 *   A list of text groups as string. Or empty to get the list of all existing
 *   text groups.
 *
 * @return array|bool
 *   The validated array of text groups. Or FALSE if the given list contains an
 *   invalid group.
 */
function _drush_locale_groups(array $groups = array()) {
  $defined_groups = &drupal_static(__FUNCTION__, NULL);
  if ($defined_groups === NULL) {
    $defined_groups = array_keys(module_invoke_all('locale', 'groups'));
  }
  if (empty($groups)) {
    return $defined_groups;
  }
  else {
    foreach ($groups as $group) {
      if (!in_array($group, $defined_groups)) {
        return drush_set_error(dt('Invalid text group: @group', array('@group' => $group)));
      }
    }
    return $groups;
  }
}

/**
 * Parses Gettext Portable Object file information and inserts into database
 *
 * @param $file
 *   Drupal file object corresponding to the PO file to import.
 * @param $langcode
 *   Language code.
 * @param $mode
 *   Should existing translations be replaced LOCALE_IMPORT_KEEP or
 *   LOCALE_IMPORT_OVERWRITE.
 * @param $group
 *   Text group to import PO file into (eg. 'default' for interface
 *   translations).
 *
 * @return array|bool
 */
function _drush_locale_import_po($file, $langcode, $mode, $group = NULL) {

  // Check if we have the language already in the database.
  if (!db_query("SELECT COUNT(language) FROM {languages} WHERE language = :language", array(':language' => $langcode))->fetchField()) {
    drush_log(dt('The language selected for import is not supported.'), 'error');
    return FALSE;
  }

  // Get strings from file (returns on failure after a partial import, or on success)
  $status = _locale_import_read_po('db-store', $file, $mode, $langcode, $group);
  if ($status === FALSE) {
    // Error messages are set in _locale_import_read_po().
    return FALSE;
  }

  // Get status information on import process.
  list($header_done, $additions, $updates, $deletes, $skips) = _locale_import_one_string('db-report');

  if (!$header_done) {
    drush_log(dt('The translation file %filename appears to have a missing or malformed header.', array('%filename' => $file->filename)), 'error');
  }

  // Clear cache and force refresh of JavaScript translations.
  _locale_invalidate_js($langcode);
  cache_clear_all('locale:', 'cache', TRUE);

  // Rebuild the menu, strings may have changed.
  menu_rebuild();

  return array(
    'add' => $additions,
    'update' => $updates,
    'delete' => $deletes,
    'skip' => $skips,
  );
}